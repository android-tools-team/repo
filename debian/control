Source: repo
Section: contrib/devel
Priority: optional
Maintainer: Android tools Maintainer <android-tools-devel@lists.alioth.debian.org>
Uploaders:
 Hans-Christoph Steiner <hans@eds.org>,
 Roger Shimizu <rosh@debian.org>
Build-Depends:
 bash-completion,
 debhelper-compat (= 12),
 dh-python,
 git (>= 1:2.20.0),
 gnupg,
 help2man,
 python3-all,
 python3-pytest,
 python3-pytest-timeout,
 python3-setuptools
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://source.android.com/source/developing.html
Vcs-Git: https://salsa.debian.org/android-tools-team/repo.git
Vcs-Browser: https://salsa.debian.org/android-tools-team/repo

Package: repo
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
 git (>= 1:2.20.0),
 gnupg,
 python3-kerberos
Recommends:
# superproject requires git 2.28
 git (>= 1:2.28.0)
Description: repository management tool built on top of git
 Repo is a repository management tool that the Android developers built on top
 of Git. Repo unifies the many Git repositories when necessary, does the
 uploads to the Android revision control system, and automates parts of the
 Android development workflow. Repo is not meant to replace Git, only to make
 it easier to work with Git in the context of Android. The repo command is an
 executable Python script that you can put anywhere in your path. In working
 with the Android source files, you will use Repo for across-network
 operations. For example, with a single Repo command you can download files
 from multiple repositories into your local working directory.
 .
 repo is an unusual tool because it downloads all of its own Python modules
 using GPG-signed git tags, and stores those files as part of the project that
 it is working with.  So this package just provides the wrapper script, which
 provides the GPG signing keys for verifying that the correct Python code was
 downloaded.
